package lambda;

public class Start {
    public static void main(String[] args) {
        Thread myThread = new Thread(() -> {
            while (true) {
                System.out.println("hello from lambda");
            }
        });
        myThread.start();
    }
}