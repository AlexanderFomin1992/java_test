package executor_with_callable_and_future;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;

public class Start {
    public static void main(String[] args) {
        Callable <String> myCallable = new MyCallable();
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future <String> future = executor.submit(myCallable);
        try {
            String result = future.get();
            System.out.println("future is done:" + String.valueOf(future.isDone()));
            System.out.println("result:" + result);
        } catch (Exception e) {

        }
        executor.shutdown();
    }
}