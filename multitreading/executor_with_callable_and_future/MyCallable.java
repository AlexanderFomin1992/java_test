package executor_with_callable_and_future;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;


public class MyCallable implements Callable<String>{
    @Override
    public String call() throws Exception{
        TimeUnit.SECONDS.sleep(1);
        return "Hello";
    }
}