package left_right_example;

public class Starter {
    public static void main(String[] args) {
        Sender sender = new Sender();
        Thread leftLegThread = new Thread(new LeftLeg(sender));
        Thread rightLegThread = new Thread(new RightLeg(sender));
        leftLegThread.start();
        rightLegThread.start();
    }
}