package left_right_example;

public class LeftLeg implements Runnable {
    private boolean isRunning = true;
    private Sender sender;
    
    public LeftLeg(Sender sender) {
        this.sender = sender;
    }

    @Override
    public void run() {
        while (this.isRunning) {
            synchronized (this.sender) {
                this.sender.send("left");
                this.sender.notifyAll();
                try {
                    this.sender.wait();
                } catch (InterruptedException e) {

                }
            }
        }
    }
}