package left_right_example;

public class RightLeg implements Runnable {
    private boolean isRunning = true;
    private Sender sender;
    
    public RightLeg(Sender sender) {
        this.sender = sender;
    }

    @Override
    public void run() {
        while (this.isRunning) {
            synchronized (this.sender) {
                this.sender.send("right");
                this.sender.notifyAll();
                try {
                    this.sender.wait();
                } catch (InterruptedException e) {

                }
            }
        }
    }
}