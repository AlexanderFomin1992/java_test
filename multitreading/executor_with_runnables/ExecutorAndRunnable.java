package executor_with_runnables;

import java.util.concurrent.*;
import java.util.List;
import java.util.Iterator;

public class ExecutorAndRunnable {
    public static void main (String [] Args) {
        ExecutorService myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.submit(new PrintRunnable());
        try {
            System.out.println("Start wait for 5 seconds");
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {

        }
        try {
            System.out.println("Start shutdown");
            myExecutor.shutdown();
            System.out.println("Start wait termination for 5 seconds");
            myExecutor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println("Task interrupted");
        } finally {
            if (myExecutor.isTerminated()) {
                System.out.println("All tasks terminated, everything OK");
            } else {
                System.out.println("Tasks not terminated, force shutdown");
                List<Runnable> waitingList = myExecutor.shutdownNow();
                System.out.println("Waiting list size: " + String.valueOf(waitingList.size()));
                Iterator<Runnable> waitingIterator = waitingList.iterator();
                while (waitingIterator.hasNext()) {
                    System.out.println(waitingIterator.next().getClass().getSimpleName());
                }
            }
        }
    }
}