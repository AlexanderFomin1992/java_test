package executor_with_runnables;

import java.time.LocalDateTime;

public class PrintRunnable implements Runnable {
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            LocalDateTime currentTime = LocalDateTime.now();
            System.err.println(Thread.currentThread().getName() + " current time: " + currentTime);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
