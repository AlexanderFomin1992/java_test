package threading_example;

public class Store {
    private int counter = 0;

    void get() {
        synchronized(this) {
            while (counter <= 0) {
                try {
                    wait();
                } catch (InterruptedException e) {

                }
            }
            counter--;
            notify();
            System.err.println(
                String.format("remove item from Store, balance: %s", 
                    counter));
        }
    }

    void put() {
        synchronized(this) {
            while (counter >= 3) {
                try{
                    wait();
                } catch (InterruptedException e) {

                }
            }
            counter++;
            System.err.println(
                String.format("add new item to Store, balance: %s",
                    counter));
            notify();
        }
    }
}