package threading_example;

public class Trade {
    public static void main(String[] args) {
        System.out.println("Hello");
        Store store = new Store();
        Producer producer = new Producer(store);
        Consumer consumer = new Consumer(store);
        Thread producerThread = new Thread(producer);
        producerThread.start();
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();
    }
}
